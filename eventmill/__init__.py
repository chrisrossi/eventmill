import itertools
import time

from collections import namedtuple
from Queue import Empty, PriorityQueue, Queue


class EventMill(object):

    def __init__(self):
        self.schedule = PriorityQueue()
        self.running = False
        self.subscribers = {}
        self.events = Queue()

    def poll(self, f, interval):
        """
        Schedule function f to run at specified interval.
        """
        self.schedule.put(_Task(time.time(), f, interval))

    def schedule(self, f, time):
        """
        Schedule function f to run at specified time.
        """
        self.schedule.put(_Task(time, f, False))

    def subscribe(self, f, cls):
        """
        Register function f as a subscriber to events of type `cls`.
        """
        self.subscribers.setdefault(cls, []).append(f)

    def notify(self, event):
        """
        Notify any subscribers of event.
        """
        self.events.put(event)

    def run(self):
        """
        Run the event loop in the current thread.
        """
        def process_tasks():
            schedule = self.schedule
            while True:
                try:
                    now = time.time()
                    task = schedule.get(block=False)
                    if task.time - now > 0:
                        # Wait until it's time
                        yield task.time
                    task.f(self)
                    if task.interval:
                        schedule.put(_Task(
                            now + task.interval, task.f, task.interval))
                except Empty:
                    # Nothing in schedule, wait 1 second
                    yield 1

        def process_events():
            events = self.events
            subscribers = self.subscribers
            try:
                while True:
                    event = events.get(block=False)
                    for cls in type(event).mro():
                        for subscriber in subscribers.get(cls, ()):
                            subscriber(event)
            except Empty:
                pass

        self.running = True
        tasks_queue = process_tasks()
        while self.running:
            sleep_until = next(tasks_queue)
            process_events()
            sleep_for = sleep_until - time.time()
            if sleep_for > 0:
                time.sleep(sleep_for)

    def stop(self):
        """
        Stop the event loop.
        """
        self.running = False

    def scan(self, *args):
        """
        Do a venusian scan to register polls and event subscribers.
        """
        raise NotImplemented


_Task = namedtuple('_Task', ['time', 'f', 'interval'])


def poll(interval):
    """
    A decorator which registers a function to be polled by an event mill.
    """
    def decorator(f):
        raise NotImplemented

    return decorator


def subscribe(cls):
    """
    A decorator which registers a function as an event subscriber for the given
    event class.
    """
    def decorator(f):
        raise NotImplemented

    return decorator


if __name__ == '__main__':
    # Demo
    import os

    class CardSwiped(object):

        def __init__(self, who):
            self.who = who

    class FileTouched(object):

        def __init__(self, name, event_factory):
            self.name = name
            self.event_factory = event_factory
            self.mtime = os.path.getmtime(name)

        def __call__(self, loop):
            now = os.path.getmtime(self.name)
            if now != self.mtime:
                self.mtime = now
                loop.notify(self.event_factory())

    def handle_card_swiped(event):
        print "Hey, %s swiped their card." % event.who

    whos = itertools.cycle(('John', 'Paul', 'George', 'Ringo'))
    loop = EventMill()
    loop.poll(FileTouched(__file__, lambda: CardSwiped(next(whos))), 0.1)
    loop.subscribe(handle_card_swiped, CardSwiped)

    print "This is a dumb example. Use 'touch' to update timestamp on this "
    print "file to simulate a card swipe."
    loop.run()
