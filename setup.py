from setuptools import setup, find_packages

requires = [
    'venusian',
    ]

tests_require = requires + [
    'pytest',
]

testing_extras = tests_require + [
    'pytest-cov',
]

setup(name='eventmill',
      version='0.1',
      description='eventmill',
      packages=find_packages(),
      include_package_data=True,
      test_suite='eventmill',
      install_requires=requires,
      extras_require={
          'testing': testing_extras,
      },
      entry_points="""\
      """,
      )
